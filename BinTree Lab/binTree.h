/*
Nicholas Perez &  Seok Hyun Kim  COSC 2336-302
Chapter 5 Lab 1 - Binary Tree Lab
Binary tree
*/
#ifndef BINTREE_H
#define BINTREE_H

#include "binNode.h"
#include "ldeque.h"
#include <string>

template <typename Key, typename E> class BinTree{
private:
  BinNode<Key,E>* root;       // Pointer to root tree node
  int size;                // Number of elements in tree
  LDeck<E> storedWords; // A deque for the imported words

  //private functions
  void clearhelp(BinNode<Key, E>*);
  BinNode<Key,E>* inserthelp(BinNode<Key,E>*, const Key&, const E&);
  //BinNode<Key,E>* deletemin(BinNode<Key, E>*);
  //BinNode<Key,E>* getmin(BinNode<Key, E>*);
  //BinNode<Key,E>* removehelp(BinNode<Key, E>*, const Key&);
  E findhelp(BinNode<Key, E>*, const Key&) const;
  void printhelp(BinNode<Key, E>*, int) const;
  
  BinNode<Key,E>* inserthelpElem(BinNode<Key,E>*, const Key&, const E&);
  Key findElemhelp(BinNode<Key, E>*, const E&) const;
  void deckInhelp(LDeck<E>);
  void deckInhelp(LDeck<E>,int);
  void deckFirstHalfhelp(LDeck<E>, int);
  int halver(LDeck<E>*);

public:
  BinTree() // Constructor 
    {
		root = NULL;
		size = 0;
	}

  ~BinTree()
  {
	  size=0;
  }      // Destructor

  void fileIn() //Reads a list of words from 'words.txt', sorts the list, and then builds the tree with them
  {	  
	  string line;
	  
	  ifstream myfile ("words.txt"); //Open the word list
	  if (myfile.is_open()) 
	  {
		cout << "Loading words from file..." << endl;
		while ( getline (myfile,line) ) //Grabs each line of the text file, stores in a deck
		{
		  //cout << "Putting the word: " << line << " in the deck." << endl; //TEMP - Prints out when each word is pulled from the text file
		  storedWords.enqueue(line); //Store each word in a deck to be sorted		 
		}
		myfile.close(); //Done reading from file
		storedWords.sortLtH(); //Sort the deck
		deckInhelp(storedWords); //Passes the sorted deck to deckInhelp which builds the tree

	  }

	  else
	  {
		  cout << "Unable to open file.";
		  abort(); //error displayed, kill the program
	  }

	  
  }
  
  
  void clear()   // Empty out the tree
    { 
		clearhelp(root); 
		root = NULL; 
		nodecount = 0; 
    }

  // Insert a record into the tree.
  // k Key value of the record.
  // e The record to insert.
  // not reworked from book example
  void insert(const Key& k, const E& e) {
    //root = inserthelp(root, k, e);
    root = inserthelpElem(root, k, e);
    size++;
  }

  // Return Record with key value k, NULL if none exist.
  // k: The key value to find. */
  // Return some record matching "k".
  // Return true if such exists, false otherwise. If
  // multiple records match "k", return an arbitrary one.
  E find(const Key& k) const 
  {
	  E temp = findhelp(root, k);
	  if (temp.empty())
	  {
		  cout << "Key: " << k << " Not found" << endl;
		  return ""; 
	  }
	  return findhelp(root, k); 
  }
  
  //As above, but returns the a key when an element is searched
  Key findElem(const E& elemVal) const
  { 
	  Key temp = findElemhelp(root, elemVal); 
	  if (temp == 0)
	  {
		  cout << "Element " << elemVal << " not found";
		  return 0;
	  }
	  return findElemhelp(root, elemVal);
  }

  // Return the number of records in the dictionary.
  int length() 
  {
	  return size;
  }
  
  void print() const
	{ // Print the contents of the BinTree
		if (root == NULL) cout << "The BST is empty.\n";
		else printhelp(root, 0);
	}

  virtual int length() const 
  {
	return size;
  }
  
  //Testing, shows the element of the root node. 
  E findRoot() const
  {
	  E it;
	  it = root->element;
	  return it;
  }
  
  //Testing, shows the elements of the root's two immediate children.
  void findRootchild(E &le, E &ri) const
  {
	  BinNode<Key,E>* temp;
	  temp = root->getLeft();
	  le = temp->element;
	  temp = root->getRight();
	  ri = temp->element;
  }
};

// Clean up BST
template <typename Key, typename E>
void BinTree<Key, E>::
clearhelp(BinNode<Key,E>* root) 
{
  if (root == NULL) return;
  clearhelp(root->getLeft());
  clearhelp(root->getRight());
  delete root;
}


// Insert a node into the BST, returning the updated tree
template <typename Key, typename E>
BinNode<Key,E>* BinTree<Key, E>::
	inserthelp(BinNode<Key,E>* root, const Key& k, const E& elem)
{
  if (root == NULL)  // Empty tree: create node
  {
	  return new BinNode<Key,E>(k, elem, NULL, NULL);
  }
  if (k < root->getKey()) //If the key of the new node is less than the root's
  {
	  root->setLeft(inserthelp(root->getLeft(), k, elem)); //Runs this method again, with the left child of the current node as the new root
  }
  else root->setRight(inserthelp(root->getRight(), k, elem)); //Key of the new node is greater than or equal to the root's, so run this method again with the right child as the new root
  return root;       // Return tree with node inserted
}

//As above, but compares the values of the element, rather than the key
template <typename Key, typename E>
BinNode<Key,E>* BinTree<Key, E>::
	inserthelpElem(BinNode<Key,E>* root, const Key& k, const E& elem)
{
  if (root == NULL)  // Empty tree: create node
  {
	  return new BinNode<Key,E>(k, elem, NULL, NULL);
  }
  if (elem < root->getElement()) //If the element of the new element is less than the root's
  {
	  root->setLeft(inserthelp(root->getLeft(), k, elem));
  }
  else root->setRight(inserthelp(root->getRight(), k, elem));
  return root;       // Return tree with node inserted
}

//Find an node's element with a given key
template <typename Key, typename E>
E BinTree<Key, E>::findhelp(BinNode<Key,E>* root,
                              const Key& k) const {
  if (root == NULL) return ""; //NULL;          // Empty tree
  if (k < root->getKey())
    return findhelp(root->getLeft(), k);   // Check left
  else if (k > root->getKey())
    return findhelp(root->getRight(), k);  // Check right
  else return root->getElement();  // Found it
}

// Find a node's key with the given element
template <typename Key, typename E>
Key BinTree<Key, E>::
	findElemhelp(BinNode<Key,E>* root, const E& elemVal) const 
{
  if (root == NULL) return NULL;          // Empty tree
  if (elemVal < root->getElement())
    return findElemhelp(root->getLeft(), elemVal);   // Check left
  else if (elemVal > root->getElement())
    return findElemhelp(root->getRight(), elemVal);  // Check right
  else return root->getKey();  // Found it
}

// Print out a BST
template <typename Key, typename E>
void BinTree<Key, E>::
printhelp(BinNode<Key,E>* root, int level) const {
  if (root == NULL) return;           // Empty tree
  printhelp(root->getLeft(), level+1);   // Do left subtree
  for (int i=0; i<level; i++)         // Indent to level
    cout << "  ";
  //cout << root->getKey() << "\n";        // Print node key
  cout << root->getElement() << endl;        // Print node key
  printhelp(root->getRight(), level+1);  // Do right subtree
}

	// Halves a deck, adds the middle element to the tree, then runs both halves again. First run.
	template <typename Key, typename E>
	void BinTree<Key, E>::
	deckInhelp(LDeck<E> inDeck)  
	{
		if (inDeck.length() == 0)
		{
			return; // Empty deck
		}
		int oldHalf = 0, half = 0, i=0;
		LDeck<E>* point = &inDeck;
		LDeck<E> firstHalf;
		half = halver(point); //Finds the half-way point for the deck
	  

	  for (int x =1; x<half; x++) //Pulls the first half of the incoming list into a holding deck
	  {
		E hold;
		hold = inDeck.dequeue();
		firstHalf.enqueue(hold); 
		cout << "Holding: " << hold <<endl;
	  }    
			E hold; 
			hold = inDeck.dequeue();
			half=half+oldHalf;
			insert(half,hold); //Inserts the half-way link into the tree, with a key set to the half-way point of the incoming deck
			cout << "Inserting into tree: " << hold << endl;

			deckFirstHalfhelp(firstHalf, oldHalf);  //Passes the held first-half of the deck to a helper function
			deckInhelp(inDeck,half); //Passes the second half of the deck to a helper function, along with the half-way point of this deck
			return;
	}

	// Halves a deck, adds the middle to the tree, then runs both halves again, uses the the previous half-way point number
	template <typename Key, typename E>
	void BinTree<Key, E>::
	deckInhelp(LDeck<E> inDeck, int oldHalf)  
	{
		if (inDeck.length() == 0)
		{
			return; // Empty deck
		}
		int half = 0, i = 0;
		LDeck<E>* point = &inDeck;
		LDeck<E> firstHalf;

		if (inDeck.length() == 1) //Only one card in the deck
		{
			E hold; //Stores the value pulled from the deck
			hold = inDeck.dequeue(); 
			half = half + oldHalf; //Makes the key for the half-way link, which is being inserted into the tree, the sum the half-way point of the current deck + sum of previous half-way points. 
			insert(half, hold); //Inserts the half-way link
			cout << "Inserting into tree: " << hold << endl;
			return;
		}
		else
		{
			half = halver(point); //Finds the half-way point of the deck
			if (half == 0) return;           // Empty deck

			for (int x = 1; x < half; x++)//Pulls the first half of the incoming list into a holding deck
			{
				E hold;
				hold = inDeck.dequeue();
				firstHalf.enqueue(hold); 
				cout << "Holding: " << hold << endl;
			}
			E hold;
			hold = inDeck.dequeue();
			half = half + oldHalf; //Makes the key for the half-way link, which is being inserted into the tree, the sum the half-way point of the current deck +sum of previous half-way points. 
			insert(half, hold); //Inserts the half-way link
			cout << "Inserting into tree: " << hold << endl;
			if (firstHalf.length() == 0) //There isn't a holding deck, because the deck you tried to split was too small
			{
				deckInhelp(inDeck, half); //Runs this function again on the remaining card in the deck
				return;
			}
			else
			{
				deckFirstHalfhelp(firstHalf, oldHalf); //Passes the held first half-deck to a helper function along with the previous half-way number
				deckInhelp(inDeck, half); //Passes the second half of the deck to a helper function along with the half-way number of this deck
				return;
			}
		}
	}

	// Halves a deck, adds the middle to the tree, then runs both halves again, uses the previous half-way point marker
	template <typename Key, typename E>
	void BinTree<Key, E>::
	deckFirstHalfhelp(LDeck<E> inDeck, int oldHalf)  
	{
		if (inDeck.length() == 0)
		{
			return; // Empty deck
		}
		int half = 0, i=0;
		LDeck<E>* point = &inDeck;
		LDeck<E> firstHalf;
		if (inDeck.length() == 1) //Only one card in the deck
		{
			E hold;
			hold = inDeck.dequeue();
			half = half + oldHalf; //Makes the key for the half-way link, which is being inserted into the tree, the sum the half-way point of the current deck + sum of previous half-way points. 
			insert(half, hold); //Inserts the one card
			cout << "Inserting into tree: " << hold << endl;
			return;
		}
		half = halver(point); //Finds the half-way point marker for this deck

	  for (int x =1; x<half; x++)//Pulls the first half of the incoming deck into a holding deck
	  {
		E hold;
		hold = inDeck.dequeue();
		firstHalf.enqueue(hold); 
		cout << "Holding: " << hold <<endl;
	  }    
			E hold; 
			hold = inDeck.dequeue();
			insert(half+oldHalf,hold); //Inserts the half-way link, sets key to half of inDeck + the previous half-way point
			cout << "Inserting into tree: " << hold << endl;
			if (firstHalf.length() == 0) //The deck you cracked in half wasn't long enough to have a holding deck
				{
					deckInhelp(inDeck,half); //Pass the remaining card to a helper function
					return;
				}
			else
			{
				deckFirstHalfhelp(firstHalf, oldHalf);  //Passes the held first half-deck to a helper function along with the previous half-way number
				deckInhelp(inDeck,half); //Passes the second half of the deck to a helper function along with the half-way number of this deck
				return;
			}
	}

	template <typename Key, typename E>int BinTree<Key, E>:: halver(LDeck<E>* checkDeck) //Finds the half-way point  for decks
	{
		int half = 0;
		if (checkDeck->length() == 0) return half;           // Empty deck
		if (checkDeck->length() % 2 == 0) // If the length is an even number, set the half-way point by just dividing by two
		{
			half = checkDeck->length() / 2;
		}
		if (checkDeck->length() % 2 == 1) // If the length is an odd number, set the half-way point by integer dividing by two and then adding one to point to the 'center'
		{
			half = (checkDeck->length() / 2) + 1;
		}
		return half;
	}

#endif