/*
Nicholas Perez &  Seok Hyun Kim  COSC 2336-302
Chapter 5 Lab 1 - Binary Tree Lab
Nodes for the binary tree
*/
#ifndef BINNODE_H
#define BINNODE_H
#define NULL 0

template <typename Key, typename E> class BinNode {
public:
  E element;      // Value for this node
  Key k;		// The node's key
  BinNode *left;        // Pointer to left child node in tree
  BinNode *right;        // Pointer to right child node in tree
  

  //Destructor
  virtual ~BinNode(){  }

  // Constructors
  BinNode (const Key& inK, const E& elemval, BinNode* leftval =NULL, BinNode* rightval =NULL) //Constructor with values
    {
		k = inK;
		element = elemval;  
		left = leftval; 
		right = rightval;
	}
  BinNode (BinNode* leftval =NULL, BinNode* rightval =NULL) //Constructor without values
	{
	  left = leftval; 
	  right = rightval;
	}
  
  virtual E& getElement() //Return the node's value
  {
	  return element;
  }
  virtual void setElement(const E& elemVal) //Set the node's value
  {
	  element = elemVal;
  }
  
  virtual Key& getKey() //Return the node's value
  {
	  return k;
  }
  virtual void setKey(const Key& inK) //Set the node's value
  {
	  k = inK;
  }

  virtual BinNode* getLeft() const //Return the node's left child
  {
	  return left;
  }
  virtual void setLeft(BinNode* changedLeft) //Set the node's left child
  {
	  left = changedLeft;
  }
  
  virtual BinNode* getRight() const //Return the node's right child
  {
	  return right;
  }
  virtual void setRight(BinNode* changedRight) //Set the node's right child
  {
	  right = changedRight;
  }

  virtual bool isLeaf() //Checks if the node is a leaf, that is to say if it has no children.
  {
	  return (left == NULL) && (right == NULL);
  }
  
};

#endif