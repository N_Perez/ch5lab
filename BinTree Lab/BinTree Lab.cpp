/*
Nicholas Perez &  Seok Hyun Kim  COSC 2336-302
Chapter 5 Lab 1 - Binary Tree Lab
*/


#include <iostream>
#include <fstream>
#include <string> 
#include "binTree.h"
#include "ldeque.h"
using namespace std;

void checkWord(string search, BinTree<int,string>&);

int main()
{
	
	BinTree<int,string> wordTree;
	wordTree.fileIn();
	string left, right;
	//Search for items
	cout << endl << "In the alphabetized list of words," << endl;
	
	checkWord("CIRNO", wordTree); 
	checkWord("ALICE", wordTree); 
	checkWord("TEWI", wordTree); 
	checkWord("KOGASA", wordTree); 
	checkWord("MARISA", wordTree); 

	cout << "The fourth position is: " <<wordTree.find(4) << endl;
	cout << "The seventh position is: " <<wordTree.find(7) << endl;
	
	string search;
	search = "SEKIBANKI"; // can replace this bit with user-input word if needed
	checkWord(search, wordTree); //Check for a word not in the list
	wordTree.find(20); //Check for a position not in the list

	
	wordTree.findRootchild(left, right);
	cout << "The root is: " << wordTree.findRoot() << " and its children are " << left << " and " << right << "." <<endl;


	wordTree.print(); // Print the tree
	system("pause");
	return 0;
}

void checkWord(string search, BinTree<int,string>& tree) // Check for a word's position in the list/if it's on the list 
{
	int check =0;
	check = tree.findElem(search);
	if ( check !=0 ) //It's in the tree
	{
		cout << search << " is in position: " << check <<endl;
	}
	else //It's not in the tree, findElem already outputs an error message
	{
		cout << endl;
	}

}

